package com.chj.thread.capt11.semantics;

import com.chj.thread.tools.SleepTools;

/**
 * 	��ʾ�����ڴ�����
 * @author chenhuajing
 *
 */
public class SynchronizedMemory {
	
	private static boolean ready;
	private static int number;
	
	private static class PrintThread extends Thread{

		@Override
		public void run() {
			while(!ready) {
				 System.out.println("number111 = "+number);
			}
			System.out.println("number222 = "+number);
		}
		
	}
	
    public static void main(String[] args) {
        new PrintThread().start();
        SleepTools.second(1);
        number = 51;
        ready = true;
        SleepTools.second(5);
        System.out.println("main is ended!");
    }
    
}
