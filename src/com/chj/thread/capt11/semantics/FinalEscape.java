package com.chj.thread.capt11.semantics;

/**
 * 	不能让final引用从构造方法中溢出
 * @author chenhuajing
 *
 */
public class FinalEscape {
	
	final int i;
	
	static FinalEscape obj;
	
	public FinalEscape() {
		this.i = 10; // 写final域
		obj = this; // this引用溢出
	}

	public static void writer() {
		new FinalEscape();
	}

	public static void reader() {
		if(obj != null) {
			int temp = obj.i;
		}
	}

}
