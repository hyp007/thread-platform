package com.chj.thread.capt07.schedule;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.chj.thread.tools.SleepTools;

public class ScheduleWorkerTime implements Runnable{
	
	public final static int Long_8 = 8;//任务耗时8秒
	public final static int Short_2 = 2;//任务耗时2秒
	public final static int Normal_5 = 5;//任务耗时5秒

	public static SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	public static AtomicInteger count = new AtomicInteger(0);

	@Override
	public void run() {
		if(count.get()==0) {
            System.out.println("Long_8....begin:"+formater.format(new Date()));
            SleepTools.second(Long_8);
            System.out.println("Long_8....end:"+formater.format(new Date())); 
            count.incrementAndGet();
    	}else if(count.get()==1) {
    		System.out.println("Short_2 ...begin:"+formater.format(new Date()));
    		SleepTools.second(Short_2);
    		System.out.println("Short_2 ...end:"+formater.format(new Date()));
            count.incrementAndGet();    		
    	}else {
    		System.out.println("Normal_5...begin:"+formater.format(new Date()));
    		SleepTools.second(Normal_5);
    		System.out.println("Normal_5...end:"+formater.format(new Date()));
    		count.incrementAndGet(); 
    	}
	}
	
	public static void main(String[] args) {
		ScheduledThreadPoolExecutor schedule = new ScheduledThreadPoolExecutor(1);
    	/*
    	 * 固定时间间隔的任务不论每次任务花费多少时间，下次任务开始执行时间从理论上讲是确定的，当然执行任务的时间不能超过执行周期。
    	 * period：任务间隔6秒，initialDelay这个定时器就是在上一个的基础上加了一个initialDelay=10000 意思就是在容器启动后，
    	 * 延迟10秒后再执行一次定时器,以后每15秒再执行一次该定时器。
    	 */
//        schedule.scheduleAtFixedRate(new ScheduleWorkerTime(), 0, 6000, TimeUnit.MILLISECONDS);
        
        /*
         * 固定延时间隔的任务是指每次执行完任务以后都延时一个固定的时间。由于操作系统调度以及每次任务执行的语句可能不同，
         * 所以每次任务执行所花费的时间是不确定的，也就导致了每次任务的执行周期存在一定的波动。
         */
        schedule.scheduleWithFixedDelay(new ScheduleWorkerTime(), 1000, 3000,  TimeUnit.MILLISECONDS);

	}

}
