package com.chj.thread.capt07;

import java.util.Random;

public class MyThreadPoolTest {

	public static void main(String[] args) throws InterruptedException {
		MyThreadPool myThreadPool = new MyThreadPool(3,0);
		myThreadPool.execute(new MyTask("task01"));
		myThreadPool.execute(new MyTask("task02"));
		myThreadPool.execute(new MyTask("task03"));
		myThreadPool.execute(new MyTask("task04"));
		myThreadPool.execute(new MyTask("task05"));
		System.out.println(myThreadPool);
	    Thread.sleep(10000);
	    myThreadPool.destory();// 所有线程都执行完成才destory
	    System.out.println(myThreadPool);
	}
	
	/*
	 * 
	 */
	static class MyTask implements Runnable{
		private String name;
		private Random r = new Random();
		
		public MyTask(String name) {
			this.name = name;
		}

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}

		@Override
		public void run() {
			try {
				Thread.sleep(r.nextInt(1000)+2000);
			} catch (InterruptedException e) {
				  System.out.println(Thread.currentThread().getId()+" sleep InterruptedException:"
	                        +Thread.currentThread().isInterrupted());
			}
			System.out.println("任务 " + name + " 完成");
		}
		
	}

}
