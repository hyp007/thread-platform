package com.chj.thread.capt06;

import java.util.Map;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 *	随机数据结构：跳表（内部元素都是有序的，跳表是一种使用空间换时间的算法）
 *	跳表的本质是同事维护了多个链表，并且链表是否分层的，与hash算法实现map不同
 *	跳表内部几个关键的数据结构：
 *	1.Node节点，	2. Index索引，内部包装了node，同时增加了向下和向右的引用	
 *	3.HeadIndex表示链表头部的第一个Index   
 * @author Administrator
 *
 */
public class ConcurrentSkipListMapDemo {
	
	
	public static void main(String[] args) {
		Map<Integer,Integer> map = new ConcurrentSkipListMap<>();
		
		for(int i=0; i< 30; i++) {
			map.put(i,i);
		}
		
		for(Map.Entry<Integer, Integer> entry: map.entrySet()) {
			System.out.println(entry.getKey()+" : "+entry.getValue());
		}
	}
	

}
