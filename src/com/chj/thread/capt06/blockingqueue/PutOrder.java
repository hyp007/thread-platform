package com.chj.thread.capt06.blockingqueue;

import java.util.concurrent.DelayQueue;

/**
 * 将订单推入队列
 * @author Administrator
 *
 */
public class PutOrder implements Runnable {

    private DelayQueue<ItemVo<Order>> queue;

    public PutOrder(DelayQueue<ItemVo<Order>> queue){
        this.queue = queue;
    }

    
	@Override
	public void run() {
		// 5秒后到期
		Order order1 = new Order("goods01",100);
		ItemVo<Order> item01 = new ItemVo<Order>(5000,order1);
		queue.offer(item01);
		System.out.println("订单5秒后超时："+order1.getOrderNo()+";"+order1.getOrderMoney());
		
		// 8秒后到期
		Order order2 = new Order("goods02",200);
		ItemVo<Order> item02 = new ItemVo<Order>(8000,order2);
		queue.offer(item02);
		System.out.println("订单8秒后超时："+order2.getOrderNo()+";"+order2.getOrderMoney());
		
	}

}
