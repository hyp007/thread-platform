package com.chj.thread.capt06.blockingqueue;

import java.util.concurrent.DelayQueue;

/**
 * 延时队列测试程序
 * @author Administrator
 *
 */
public class TestOrder {

	public static void main(String[] args) throws InterruptedException {
		// 延时队列
		DelayQueue<ItemVo<Order>> queue = new DelayQueue<ItemVo<Order>>();
		
		new Thread(new PutOrder(queue)).start();
		new Thread(new FetchOrder(queue)).start();
		
		 //每隔500毫秒，打印个数字
        for(int i=1;i<15;i++){
            Thread.sleep(500);
            System.out.println(i*500);
        }
	}

}
