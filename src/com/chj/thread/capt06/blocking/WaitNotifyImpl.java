package com.chj.thread.capt06.blocking;

import java.util.LinkedList;
import java.util.List;

public class WaitNotifyImpl<E> implements IBoundedBuffer<E> {
	// 模拟阻塞队列存放任务
	private List queue = new LinkedList();
	// 最多存放限制
	private final int limit;

	public WaitNotifyImpl(int limit) {
		this.limit = limit;
	}


	@Override
	public synchronized void put(E x) throws InterruptedException {
		while(queue.size() >= limit) {
			wait();
		}
		queue.add(x);
		notifyAll();
	}


	@Override
	public synchronized E take() throws InterruptedException {
		while(queue.size() == 0) {
			wait();
		}
		E result = (E)queue.remove(0);
		notifyAll();
		return result;
	}

}
