package com.chj.thread.capt08.transfer.service;

import java.util.Random;

import com.chj.thread.capt08.transfer.UserAccount;

/**
 * 不会产生死锁的安全转账第二种方法
 * @author Administrator
 *
 */
public class SafeOperateToo implements ITransfer {

	@Override
	public void transfer(UserAccount from, UserAccount to, int amount) throws InterruptedException {
		
		Random r = new Random();
		while(true) {
			if(from.getLock().tryLock()) {
				System.out.println(Thread.currentThread().getName() +" get"+from.getName());
				try {
					 System.out.println(Thread.currentThread().getName() +" get"+to.getName());
                     from.reduceMoney(amount);
                     to.addMoney(amount);
                     System.out.println(from);
                     System.out.println(to);
                     break;
				}finally {
					from.getLock().unlock();
				}
				
			}
		}
		
	}
	
	

}
