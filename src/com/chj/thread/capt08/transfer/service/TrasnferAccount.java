package com.chj.thread.capt08.transfer.service;

import com.chj.thread.capt08.transfer.UserAccount;

public class TrasnferAccount implements ITransfer{

	@Override
	public void transfer(UserAccount from, UserAccount to, int amount) throws InterruptedException {
		
		synchronized (from){
            System.out.println(Thread.currentThread().getName()+" get "+from.getName());
            Thread.sleep(100);
            synchronized (to){
                System.out.println(Thread.currentThread().getName()+" get "+to.getName());
                from.reduceMoney(amount);
                to.addMoney(amount);
                System.out.println("from=="+from);
                System.out.println("to==="+to);
            }
        }
		
	}

}
