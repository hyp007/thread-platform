package com.chj.thread.capt08.transfer;

import com.chj.thread.capt08.transfer.service.ITransfer;
import com.chj.thread.capt08.transfer.service.SafeOperate;
import com.chj.thread.capt08.transfer.service.SafeOperateToo;
import com.chj.thread.capt08.transfer.service.TrasnferAccount;

public class PayCompany {
	
	/*执行转账动作的线程*/
    private static class TransferThread extends Thread{

        private String name;
        private UserAccount from;
        private UserAccount to;
        private int amount;
        private ITransfer transfer;

        public TransferThread(String name, UserAccount from, UserAccount to,
                              int amount, ITransfer transfer) {
            this.name = name;
            this.from = from;
            this.to = to;
            this.amount = amount;
            this.transfer = transfer;
        }


        public void run(){
            Thread.currentThread().setName(name);
            try {
                transfer.transfer(from,to,amount);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
    
    public static void main(String[] args) throws InterruptedException {
        PayCompany payCompany = new PayCompany();
        UserAccount uAcount = new UserAccount("zhangsan",20000);
        UserAccount uAcount2 = new UserAccount("lisi",20000);
        // 转账操作1
        ITransfer transfer = new TrasnferAccount();
        // 转账操作2
        ITransfer transfer2 = new SafeOperate();
        // 转账操作3
        ITransfer transfer3 = new SafeOperateToo();
        
        // 在场景1里面 由于转账顺序不一样导致锁死出现
        TransferThread zhangsanToLisi = new TransferThread("zhangsanToLisi",uAcount,uAcount2,2000,transfer);
        TransferThread lisiToZhangsan = new TransferThread("lisiToZhangsan",uAcount2,uAcount,4000,transfer);
        
        zhangsanToLisi.start();
//        Thread.sleep(200);  // 转账操作1情况， 需要让两次转账时间错开 ，避免死锁产生
        lisiToZhangsan.start();
    }
    
}
