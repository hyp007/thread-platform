package com.chj.thread.capt08.singleton;

/**
 *	 在域上运用延迟初始化占位类模式
 * @author Administrator
 *
 */
public class InstanceLazy {
	
	private Integer value;
	private Integer heavy;//成员变量很耗资源， ;
	
    public InstanceLazy(Integer value) {
		super();
		this.value = value;
	}
    
    public Integer getValue() {
		return value;
	}

	public Integer getVal() {
		return InstanceHolder.val;
	}
	
	private static class InstanceHolder{
        public static Integer val = new Integer(100);
    }

}
