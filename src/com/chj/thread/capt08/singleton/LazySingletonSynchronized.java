package com.chj.thread.capt08.singleton;

/**
 * 	单列模式： synchronized实现方式
 * @author Administrator
 *
 */
public class LazySingletonSynchronized {
	
	// 通过private修饰，就是为了防止这个类被随意创建
	private LazySingletonSynchronized(){
		System.out.println("LazySingleton is created!!");
	}
	
	// 首先instance对象必须是private并且static，如果不是private那么instance的安全无法得到保证
	// 其次因为工厂方法必须是static，因此变量instance也必须是static
	private static LazySingletonSynchronized instance = null;
	
	// 为了防止对象被多次创建，使用synchronized关键字进行同步（缺点是并发环境下加锁，竞争激励的场合可能对性能产生一定影响）
	public static synchronized LazySingletonSynchronized getInstance() {
		if(instance == null) {
			instance = new LazySingletonSynchronized();
		}
		return instance;
	}

}
