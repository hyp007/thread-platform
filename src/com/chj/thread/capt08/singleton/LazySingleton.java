package com.chj.thread.capt08.singleton;

/**
 * 	单列模式： 无锁的实现方式
 * @author Administrator
 *
 */
public class LazySingleton {
	
	// 通过private修饰，就是为了防止这个类被随意创建
	private LazySingleton(){
		System.out.println("LazySingleton is created!!");
	}
	
	// 利用虚拟机的类初始化机制创建单例
	private static class SingletonHolder {
		private static LazySingleton instance = new LazySingleton();
	}
	
	// 为了防止对象被多次创建，使用synchronized关键字进行同步（缺点是并发环境下加锁，竞争激励的场合可能对性能产生一定影响）
	public static LazySingleton getInstance() {
		return SingletonHolder.instance;
	}

}
