package com.chj.thread.capt08.safepublish;

/**
 * 对于我们自己使用或者声明的类，JDK自然没有提供这种包装类的办法，但是我们可以仿造这种模式或者委托给线程安全的类，
 * 当然，对这种通过get等方法发布出去的对象，最根本的解决办法还是应该在实现上就考虑到线程安全问题
 */
public class SafePublish {
	private int i;

    public SafePublish() {
    	i = 2;
    }
	
	public int getI() {
		return i;
	}

	public static void main(String[] args) {
		SafePublish safePublish = new SafePublish();
		int j = safePublish.getI();
		System.out.println("before j="+j);
		j = 3;
		System.out.println("after j="+j);
		System.out.println("getI = "+safePublish.getI());
	}
}
