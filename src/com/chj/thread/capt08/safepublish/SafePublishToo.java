package com.chj.thread.capt08.safepublish;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 我们在发布这对象出去的时候，就应该用线程安全的方式包装这个对象。
 * 对于我们自己使用或者声明的类，JDK自然没有提供这种包装类的办法，但是我们可以仿造这种模式或者委托给线程安全的类，
 * 当然，对这种通过get等方法发布出去的对象，最根本的解决办法还是应该在实现上就考虑到线程安全问题，
 * 参见代码包cn.enjoyedu.ch7.safeclass.safepublish下的代码
 */
public class SafePublishToo {
	// 包装成为线程安全的list
    private List<Integer> list = Collections.synchronizedList(new ArrayList<>(3));

    public SafePublishToo() {
        list.add(1);
        list.add(2);
        list.add(3);
    }

    public List getList() {
        return list;
    }

    public static void main(String[] args) {
        SafePublishToo safePublishToo = new SafePublishToo();
        List<Integer> list = safePublishToo.getList();
        System.out.println(list);
        list.add(4);
        System.out.println(list);
        System.out.println(safePublishToo.getList());
    }
}
