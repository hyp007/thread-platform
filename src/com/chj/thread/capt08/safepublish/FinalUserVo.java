package com.chj.thread.capt08.safepublish;

/**
 * 类中持有的成员变量，如果是基本类型，发布出去，并没有关系，因为发布出去的其实是这个变量的一个副本
 * @author Administrator
 *
 */
public final class FinalUserVo {
	
	private int age;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}
