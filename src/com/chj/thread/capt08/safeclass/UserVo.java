package com.chj.thread.capt08.safeclass;

public class UserVo {
	private int age;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
