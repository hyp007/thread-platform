package com.chj.thread.capt08.safeclass;

/**
 *  类不可变
 *  加final关键字，对于一个类，所有的成员变量应该是私有的，同样的只要有可能，所有的成员变量应该加上final关键字，
 *  但是加上final，要注意如果成员变量又是一个对象时，这个对象所对应的类也要是不可变，才能保证整个类是不可变的
 *  
 *  volatile方式：并不能保证类的线程安全性，只能保证类的可见性，最适合一个线程写，多个线程读的情景。
 *  加锁和CAS：
 *  我们最常使用的保证线程安全的手段，使用synchronized关键字，使用显式锁，使用各种原子变量，修改数据时使用CAS机制等等。

 *
 */
public class ImmutableClass {
	

	private final int a;
	
	private final UserVo user = new UserVo();//不安全
	 
	public int getA() {
		return a;
	}
	public UserVo getUser() {
		return user;
	}
	
	public ImmutableClass(int a) {
		this.a = a;
	}
	
	
	public static class User{
    	private int age;

		public int getAge() {
			return age;
		}

		public void setAge(int age) {
			this.age = age;
		}
    }
	 
	    
}
