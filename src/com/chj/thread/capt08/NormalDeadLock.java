package com.chj.thread.capt08;

/**
 * 演示普通账户的死锁和解决
 * @author Administrator
 * 死锁是指两个或两个以上的进程在执行过程中，由于竞争资源或者由于彼此通信而造成的一种阻塞的现象，
 * 若无外力作用，它们都将无法推进下去。此时称系统处于死锁状态或系统产生了死锁。
 * 两种解决方式：
 * 1、内部通过顺序比较，确定拿锁的顺序；2、采用尝试拿锁的机制。
 *
 */
public class NormalDeadLock {
	
	private static Object valueFirst = new Object(); // 第一个锁
	private static Object valueSecond = new Object(); // 第二个锁
	
	// 先拿第一个锁，再拿第二个锁
	private static void fisrtToSecond() throws InterruptedException {
		String threadName = Thread.currentThread().getName();
		synchronized(valueFirst) {
			System.out.println(threadName + "get first  lock end!");
			Thread.sleep(100);
			synchronized(valueSecond) {
				System.out.println(threadName+" get second lock end!");
			}
		}
	}

	//先拿第二个锁，再拿第一个锁（产生死锁，可以通过调整锁的顺序解决）
    private static void SecondToFisrt() throws InterruptedException {
        String threadName = Thread.currentThread().getName();
        synchronized (valueFirst){
            System.out.println(threadName+" get first lock end");
            Thread.sleep(100);
            synchronized (valueSecond){
                System.out.println(threadName+" get second lock  end");
            }
        }
    }
    
    private static class TestThread extends Thread{
        private String name;
        public TestThread(String name) {
            this.name = name;
        }

        public void run(){
            Thread.currentThread().setName(name);
            try {
                SecondToFisrt();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    
    
	public static void main(String[] args) {
		Thread.currentThread().setName("TestDeadLock");
        TestThread testThread = new TestThread("SubTestThread");
        testThread.start();
        try {
            fisrtToSecond();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
	}

}
