package com.chj.thread.capt02.forkjoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

import com.chj.thread.tools.SleepTools;

/**
 * fork-join 执行累加计算
 * 我们要使用Fork-Join框架，必须首先创建一个Fork-Join任务。
 * 它提供在任务中执行fork和join的操作机制，通常我们不直接继承ForkjoinTask类，只需要直接继承其子类
 * 1. RecursiveAction，用于没有返回结果的任务  	 
 * 2. RecursiveTask，用于有返回值的任务
 * @author Administrator
 *
 */
public class ForkJoinSumArray {
	
	// RecursiveTask<>
	private static class SumTask extends RecursiveTask<Integer>{
		private static final long serialVersionUID = 1L;
		// 阈值
		private final static int THRESHOLD = MakeArray.ARRAY_LENGTH/10;
		private int[] src;
		private int fromIndex;
		private int toIndex;

		public SumTask(int[] src, int fromIndex, int toIndex) {
			super();
			this.src = src;
			this.fromIndex = fromIndex;
			this.toIndex = toIndex;
		}

		/**
		 * 在我们自己实现的compute方法里，首先需要判断任务是否足够小，如果足够小就直接执行任务.
		 * 如果不足够小，就必须分割成两个子任务，每个子任务在调用invokeAll方法时，又会进入compute方法，
		 * 看看当前子任务是否需要继续分割成孙任务，如果不需要继续分割，则执行当前子任务并返回结果。
		 * 使用join方法会等待子任务执行完并得到其结果
		 */
		@Override
		protected Integer compute() {
			// 首先判断任务是否足够小，如果足够小就直接执行任务
			if(toIndex-fromIndex < THRESHOLD) {
				// 将任务结果交给上一级
				int count = 0;
				for(int i = fromIndex; i<= toIndex; i++) {
					SleepTools.ms(1);
					count = count + src[i];
				}
				return count;
			}else { // 
				int mid = (fromIndex + toIndex) / 2;
				SumTask leftTask = new SumTask(src, fromIndex, mid);
				SumTask rightTask = new SumTask(src, mid+1, toIndex);
				// 任务拆分完城后调用invokeAll（）方法将子任务放进去，继续计算判断是否需要再次拆分，直到拆分达到阈值
				invokeAll(leftTask,rightTask);
				// 返回计算结果
				return leftTask.join() + rightTask.join();
			}
		}
		
	}

	/**
	 * Task要通过ForkJoinPool来执行，使用submit 或 invoke 提交
	 * 两者的区别是：invoke是同步执行，调用之后需要等待任务完成，才能执行后面的代码；submit是异步执行。
	 * @param args
	 */
	public static void main(String[] args) {
		// 创建ForkJoinPool来执行任务
		ForkJoinPool pool = new ForkJoinPool();
		int[] src = MakeArray.makeArray();
		// 创建fork-join任务
		SumTask innerTask = new SumTask(src, 0, src.length-1);
		long start = System.currentTimeMillis();
		// invoke是同步执行，调用之后需要等待任务完成，才能执行后面的代码
		pool.invoke(innerTask);
		//使用join方法会等待子任务执行完并得到其结果 result = innerTask.join();
		int result = innerTask.join();
		System.out.println("The count result is: "+result+" spend time:"+(System.currentTimeMillis()-start)+"ms");
	}

}
