package com.chj.thread.capt02.forkjoin;

import java.util.Arrays;

/**
 * 归并排序
 * @author Administrator
 *
 */
public class MergeSort {
	
	public static int[] sort(int[] array) {
		// 执行拆分的最小阈值
		if(array.length <= MakeArray.THRESHOLD) {
			return simpleInsertSort(array);
		}else {
			// 切割数组，然后递归调用
			int mid = array.length / 2;
			int leftArray[]  = Arrays.copyOfRange(array, 0, mid);
			int rightArray[]  = Arrays.copyOfRange(array, mid, array.length);
			return merge(sort(leftArray),sort(rightArray));
		}
	}
	
	/**
	 * 归并排序——将两段排序好的数组结合成一个排序数组
	 */
	public static int[] merge(int[] left, int[] right) {
		// 
		int[] result = new int[left.length + right.length];
		for(int index = 0, i = 0, j = 0; index < result.length; index++) {
			// 如果左边数组已经取完，取右边数组的值即可
			if(i >= left.length) {
				result[index] = right[j++];
			}else if(j >= right.length) {// 右边数组值取完，完全取左边的数组值即可
				result[index] = left[i++];
			}else if(left[i] > right[j]) {// 左边数组的元素值大于右边数组，取右边数组的值
				result[index] = right[j++];
			}else {// 右边数组的元素值大于右边数组，取右=左边数组的值
				result[index] = left[i++];
			}
		}
		return result;
	}

	/**
	 * 简单的插入排序
	 * @param array
	 * @return
	 */
	public static int[] simpleInsertSort(int[] array){
		if(array.length == 0) {
			return array;
		}
		// 当前排序的数据，该元素之前的元素均已被排过序
		int currentValue;
		for(int i = 0; i < array.length-1; i++) {
			int preIndex = i; // 已经排序数据的索引
			currentValue = array[preIndex + 1];
			// 在已经被排序过的数据中倒序寻找合适的位置，如果当前待排序数据小于比较的元素，则将比较的元素后移一位
			while(preIndex >=0 && currentValue < array[preIndex]) {
				array[preIndex + 1] = array[preIndex];
				preIndex--;
			}
			// 循环结束时，说明已经找到了当前待排序数据的合适位置，插入进去
			array[preIndex + 1] = currentValue;
		}
		return array;
		
	}

	public static void main(String[] args) {
		  System.out.println("============================================");
//		  int[] arrays = simpleSort(MakeArray.makeArray());
		  long start = System.currentTimeMillis();
	      MergeSort.sort(MakeArray.makeArray());
	      System.out.println(" spend time:"+(System.currentTimeMillis()-start)+"ms");

	}

}
