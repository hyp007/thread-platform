package com.chj.thread.capt02.forkjoin;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

/**
 * fork-join实现的归并排序
 * 我们要使用Fork-Join框架，必须首先创建一个Fork-Join任务。
 * 它提供在任务中执行fork和join的操作机制，通常我们不直接继承ForkjoinTask类，只需要直接继承其子类
 * 1. RecursiveAction，用于没有返回结果的任务  	 
 * 2. RecursiveTask，用于有返回值的任务
 * @author Administrator
 *
 */
public class ForkJoinSort {
	
	private static class SumTask extends RecursiveTask<int[]>{
		private static final long serialVersionUID = 1L;
		private final static int THRESHOLD = 2;
		private int[] src;
		
		public SumTask(int[] src) {
			super();
			this.src = src;
		}

		/**
		 * 在我们自己实现的compute方法里，首先需要判断任务是否足够小，如果足够小就直接执行任务.
		 * 如果不足够小，就必须分割成两个子任务，每个子任务在调用invokeAll方法时，又会进入compute方法，
		 * 看看当前子任务是否需要继续分割成孙任务，如果不需要继续分割，则执行当前子任务并返回结果。
		 * 使用join方法会等待子任务执行完并得到其结果
		 */
		@Override
		protected int[] compute() {
			// 首先判断任务是否足够小，如果足够小就直接执行任务
			if(src.length <= THRESHOLD) {
				// 将任务结果交给上一级
				return MergeSort.simpleInsertSort(src);
			}else { // 
				int mid = src.length / 2;
				SumTask leftTask = new SumTask(Arrays.copyOfRange(src, 0, mid));
				SumTask rightTask = new SumTask(Arrays.copyOfRange(src, mid, src.length));
				// 任务拆分完城后调用invokeAll（）方法将子任务放进去，继续计算判断是否需要再次拆分，直到拆分达到阈值
				invokeAll(leftTask,rightTask);
				// 将所有子任务通过join串行起来，并返回结果
				int[] leftResult = leftTask.join();
				int[] rightResult = rightTask.join();
				// 返回计算结果
				return MergeSort.merge(leftResult, rightResult);
			}
		}
		
	}

	/**
	 * Task要通过ForkJoinPool来执行，使用submit 或 invoke 提交
	 * 两者的区别是：invoke是同步执行，调用之后需要等待任务完成，才能执行后面的代码；submit是异步执行。
	 * @param args
	 */
	public static void main(String[] args) {
		// 创建ForkJoinPool来执行任务
		ForkJoinPool pool = new ForkJoinPool();
		int[] src = MakeArray.makeArray();
		// 创建fork-join任务
		SumTask innerTask = new SumTask(src);
		long start = System.currentTimeMillis();
		// invoke是同步执行，调用之后需要等待任务完成，才能执行后面的代码
		pool.invoke(innerTask);
	    System.out.println(" spend time:"+(System.currentTimeMillis()-start)+"ms");
	}

}
