package com.chj.thread.capt02.overtime;

import java.sql.Connection;
import java.util.LinkedList;

/**
 * 	实现一个数据库的连接池 (超时处理)
 * @author Administrator
 *
 */
public class DBTool {

	//数据库池的容器
	private static LinkedList<Connection> pool = new LinkedList<>();
	
	// 如果连接数未满 则继续添加
	public DBTool(int initalSize) {
		if(initalSize > 0) {
			for(int i=0;i<initalSize;i++) {
				pool.addLast(SqlConnectImpl.fetchConnection());
			}
		}
	}
	
	// 在mills时间内还拿不到数据库连接，返回null
	public Connection fetchConn(long mills) throws InterruptedException {
		synchronized(pool) {
			if(mills < 0) {
				while(pool.isEmpty()) {
					pool.wait();
				}
				return pool.removeFirst();
			}else {
				// 假设  等待时间时长为mills，当前时间now+mills以后超时
				long overtime = System.currentTimeMillis() + mills;
				// 等待的持续时间
				long remain = mills;
				while(pool.isEmpty() && remain >0) {
					pool.wait();
					// 等待剩下的持续时间
					remain = overtime - System.currentTimeMillis();
				}
				Connection result  = null;
				if(!pool.isEmpty()) {
					result = pool.removeFirst();
				}
				return result;
			}
		}
		
	}
	
	
	// 放回数据库连接
	public void releaseConnection(Connection conn) {
		
		if(conn != null) {
			synchronized(pool) {
				pool.addLast(conn);
				pool.notifyAll();
			}
		}
	}
	
	
	
	
}
