package com.chj.thread.capt02.semaphore;

import java.sql.Connection;
import java.util.LinkedList;
import java.util.concurrent.Semaphore;

/**
 * 演示Semaphore用法，一个数据库连接池的实现
 * @author Administrator
 *
 */
public class DBPoolSemaphore {

	private final static int POOL_SIZE = 10;
	// useful表示可用的数据库连接，useless表示已用的数据库连接
	private final Semaphore useful,useless;
	
	public DBPoolSemaphore() {
		this.useful =  new Semaphore(POOL_SIZE);;
		this.useless = new Semaphore(0);
	}
	
	//存放数据库连接的容器
	private static LinkedList<Connection> pool = new LinkedList<Connection>();
	// 初始化池
	static {
		for (int i = 0; i < POOL_SIZE; i++) {
			pool.addLast(SqlConnectImpl.fetchConnection());
		}
	}
	
	// 释放数据库连接
	public void returnConnect(Connection connection) throws InterruptedException {
		if(connection!=null) {
			System.out.println("当前有"+useful.getQueueLength()+"个线程等待数据库连接！！"
					+"可用连接数:"+useful.availablePermits());
			// 空闲个数增加
			useless.acquire();
			synchronized (pool) {
				pool.addLast(connection);
			}
			// 使用连接减少
			useful.release();
		}
	}
	
	// 从池子拿连接
	public Connection takeConnect() throws InterruptedException {
		// 使用连接数增加
		useful.acquire();
		Connection conn;
		synchronized (pool) {
			conn = pool.removeFirst();
		}
		// 空闲连接数减少
		useless.release();
		return conn;
	}
	
	
}
