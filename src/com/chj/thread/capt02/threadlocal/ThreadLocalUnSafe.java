package com.chj.thread.capt02.threadlocal;

import com.chj.thread.tools.SleepTools;

/**
 * ThreadLocal的线程不安全演示
 * @author Administrator
 *
 */
public class ThreadLocalUnSafe implements Runnable{
	
	 public Number number = new Number(0);
	
	 private static ThreadLocal<Number> numLocal = new ThreadLocal<Number>() {};

	@Override
	public void run() {
		// 每个线程计数加一
		number.setNum(number.getNum()+1);
		// 将其存储到ThreadLocal
		numLocal.set(number);
		SleepTools.ms(2);
		// 输出num值
		System.out.println(Thread.currentThread().getName() + "=" + numLocal.get().getNum());

	}

	public static void main(String[] args) {
		for (int i = 0; i < 5; i++) {
			new Thread(new ThreadLocalUnSafe()).start();;
		}
	}

	 private static class Number {
	        public Number(int num) {
	            this.num = num;
	        }

	        private int num;

	        public int getNum() {
	            return num;
	        }

	        public void setNum(int num) {
	            this.num = num;
	        }

	        @Override
	        public String toString() {
	            return "Number [num=" + num + "]";
	        }
	    }

	

}
