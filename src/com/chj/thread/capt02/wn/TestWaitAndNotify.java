package com.chj.thread.capt02.wn;

/**
 * 	测试wait/notify/notifyAll
 * @author Administrator
 *
 */
public class TestWaitAndNotify {
	
	private static Express express = new Express(0,Express.CITY);
	
	// 检查里程变化得线程类，不满足条件则一直等待
	private static class CheckMileage extends Thread{
		@Override
        public void run() {
			express.waitMileage();
		}
	}

	// 检查里程变化得线程类，不满足条件则一直等待
	private static class CheckSite extends Thread{
		@Override
        public void run() {
			express.waitSite();
		}
	}

	public static void main(String[] args) throws InterruptedException {
		for(int i=0;i<3;i++){//三个线程
            new CheckSite().start();
        }
        for(int i=0;i<3;i++){//里程数的变化
            new CheckMileage().start();
        }

        Thread.sleep(2000);
        express.changeSite();
        express.changeMileage();//快递地点或者里程点变化
	}
}
