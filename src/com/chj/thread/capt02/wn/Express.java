package com.chj.thread.capt02.wn;

/**
 * 	类说明：快递实体类
 * @author Administrator
 *
 */
public class Express {

	public final static String CITY = "ShangHai";
	/* 快递运输里程数 */
	private int mileage;
	/* 快递到达地点 */
	private String site;

	public Express() {
	}

	public Express(int mileage, String site) {
		this.mileage = mileage;
		this.site = site;
	}
	
	// 里程变化方法，通知处于wait状态并需要处理路程公里变化得线程进行业务处理
	public synchronized void changeMileage() {
		this.mileage = 101;
//		notify();
		notifyAll();
		//其他业务处理
	}
	
	// 地点变更，通知处于wait状态并且需要处理地点变更得线程进行业务处理
	public synchronized void changeSite() {
		this.site = "BeiJing";
//    	notify();
    	notifyAll();
	}
	
	// 处理里程变换得方法
	public synchronized void waitMileage() {
		// 如果里程小于100，进入等待
		while(this.mileage <= 100) {
			try {
				wait();
				System.out.println("waitMileage thread["+Thread.currentThread().getId()+"] has been notifed.");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("the mileage is "+this.mileage+", I will change db.");
	}
	
	// 处理地点变换得方法
	public synchronized void waitSite() {
		// 如果里程小于100，进入等待
		while(CITY.equals(this.site)) {
			try {
				wait();
				System.out.println("waitSite thread["+Thread.currentThread().getId()+"] has been notifed.");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("the site is"+this.site+",I will call user.");
	}

	
	
	
	
	
	
	

}
