package com.chj.thread.capt12.flow;


import java.util.concurrent.CompletableFuture;

import com.chj.thread.tools.SleepTools;

/**
 * 	取最快消费类
 */
public class AcceptEither {
    public static void main(String[] args) {
        CompletableFuture.supplyAsync(() -> {
            SleepTools.second(1);
            return "s1";
        }).acceptEither(CompletableFuture.supplyAsync(() -> {
            SleepTools.second(2);
            return "hello world";
        }), (s)-> System.out.println(s));
        SleepTools.second(3);
    }
}
