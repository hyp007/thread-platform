package com.chj.thread.capt12.lambda;

import java.util.ArrayList;
import java.util.List;

import com.chj.thread.capt12.lambda.Step2.CircleByRed;

/**
 *	 使用内部匿名类和Lambda表达式
 * @author chenhuajing
 *
 */
public class Step3 {

	/**
	 * 	 定义挑选圆的行为接口
	 */
	interface ChoiceCircle{
		boolean getCircle(Circle circle);
	}
	
	/**
	 * 	行为参数化，根据条件挑选出圆
	 */
	public static List<Circle> getCircleByChoice(List<Circle> circles, ChoiceCircle choice){
		List<Circle> circleList = new ArrayList<>();
		for(Circle circle : circles) {
			if(choice.getCircle(circle)) {
				circleList.add(circle);
			}
		}
		return circleList;
	}
	
	/**
	 * 
	 */
	public static void service() {
		List<Circle> src = new ArrayList<>(); // 待处理的圆的集合
		// 选出半径为2的圆
        List<Circle> radiusTwos =  getCircleByChoice(src, new ChoiceCircle() {
			@Override
			public boolean getCircle(Circle circle) {
				return circle.getRadius() == 2;
			}
        });
        // 选出红色的圆
        List<Circle> red =  getCircleByChoice(src, new ChoiceCircle() {
			@Override
			public boolean getCircle(Circle circle) {
				return "Red".equals(circle.getColor());
			}
        });
	}
	
	//
	public static void serviceByLambda() {
		List<Circle> src = new ArrayList<>(); // 待处理的圆的集合
		// 选出半径为2的圆
        List<Circle> radiusTwos =  getCircleByChoice(src, 
        		(Circle circle) -> circle.getRadius() ==2);
        // 选出红色的圆
        List<Circle> redCircle =  getCircleByChoice(src, 
        		circle -> "Red".equals(circle.getColor()));
	}
	
	
}
