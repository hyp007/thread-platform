package com.chj.thread.capt12.lambda;

/**
 * 	ʵ����
 * @author chenhuajing
 *
 */
public class Circle {
	private int radius;
    private String color;
    private int x;
    private int y;

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
