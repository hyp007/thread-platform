package com.chj.thread.capt12.function;

import java.util.Arrays;

/**
 *  Java8: 函数编程，不变的对象
 */
public class FunctionDemo1 {

    public static void main(String[] args) {
         int[] arry = {1,2,3,4,5,6,7,8,9,10};
         // 函数式编程中，几乎所有传递的对象都不会被轻易修改
        Arrays.stream(arry).map((x) -> (x=x+1)).forEach(System.out::println);
        System.out.println();
        Arrays.stream(arry).forEach(System.out::println);
    }

}
