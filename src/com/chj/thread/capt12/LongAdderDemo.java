package com.chj.thread.capt12;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;

/**
 * LongAdder,原子类以及同步锁性能测试
 * @author chenhuajing
 *
 */
public class LongAdderDemo {
	
	private static final int MAX_THREADS = 20;
	private static final int TASK_COUNT = 400;
	private static final int TARGET_COUNT = 100000000;	
	
	// 三个不同类型的long有关的变量
	private long count = 0;
	private AtomicLong atomicCount = new AtomicLong(0L);
	private LongAdder laCount = new LongAdder();
	
	// 控制线程同时进行
	private static CountDownLatch cdlsyn = new CountDownLatch(TASK_COUNT);
	private static CountDownLatch cdlAtomic = new CountDownLatch(TASK_COUNT);
	private static CountDownLatch cdlAdder = new CountDownLatch(TASK_COUNT);
	
	// 普通long的同步锁测试方法
	protected synchronized long inc() {
		return ++count;
	}
	
	protected synchronized long getCount() {
		return count;
	}
	
	/*
	 * 	1、普通long的同步锁测试任务
	 */
	public class SyncTask implements Runnable{
		protected String name;
		protected long startTime;
		LongAdderDemo adderOut;

		public SyncTask(long startTime, LongAdderDemo adderOut) {
			this.startTime = startTime;
			this.adderOut = adderOut;
		}

		@Override
		public void run() {
			long value = adderOut.getCount();
			while(value < TARGET_COUNT) {
				value = adderOut.inc();
			}
			long endTime = System.currentTimeMillis();
            System.out.println("SyncTask spend:" + (endTime - startTime) + "ms" );
            // 计数器的初始值为初始任务的数量。每当完成了一个任务后，计数器的值就会减1
            cdlsyn.countDown();
		}
	}
	
	/*
	 * 	1、普通long的执行同步锁测试
	 */
	public void testSync() throws InterruptedException {
		ExecutorService executor = Executors.newFixedThreadPool(MAX_THREADS);
		long starttime = System.currentTimeMillis();
		SyncTask task = new SyncTask(starttime,this);
		for(int i = 0; i < TASK_COUNT; i++) {
			executor.submit(task);
		}
		//当计数器值到达0时，它表示所有的已经完成了任务，然后在闭锁上等待CountDownLatch.await()方法的线程就可以恢复执行任务。
		cdlsyn.await(); 
		// 保证所有任务执行完毕后再关系线程池
		executor.shutdown();
	}
	
	
	/**
	 * 	2、原子型long的测试任务
	 */
	public class AtomicTask implements Runnable{
		protected String name;
		protected long startTime;
		
		public AtomicTask(long startTime) {
			this.startTime = startTime;
		}

		@Override
		public void run() {
			long value = atomicCount.get();
			while(value < TARGET_COUNT) {
				value = atomicCount.incrementAndGet();
			}
			long endTime = System.currentTimeMillis();
            System.out.println("AtomicTask spend:" + (endTime - startTime) + "ms" );
            cdlAtomic.countDown();
		}
	}
	
	/*
	 * 	2、原子型long的执行测试
	 */
	public void testAtomic() throws InterruptedException {
		ExecutorService executor = Executors.newFixedThreadPool(MAX_THREADS);
		long starttime = System.currentTimeMillis();
		AtomicTask task = new AtomicTask(starttime);
		for(int i = 0; i < TASK_COUNT; i++) {
			executor.submit(task);
		}
		cdlAtomic.await(); 
		executor.shutdown();
	}
	
	/**
	 * 	3、LongAdder的测试任务
	 */
	public class LongAdderTask implements Runnable{
		protected String name;
		protected long startTime;
		
		public LongAdderTask(long startTime) {
			this.startTime = startTime;
		}

		@Override
		public void run() {
			long value = laCount.sum();
			while(value < TARGET_COUNT) {
				laCount.increment();
				value = laCount.sum();
			}
			long endTime = System.currentTimeMillis();
            System.out.println("LongAdderTask spend:" + (endTime - startTime) + "ms" );
            cdlAdder.countDown();
		}
	}
	
	/*
	 * 	3、LongAdder的执行测试
	 */
	public void testLongAdder() throws InterruptedException {
		ExecutorService executor = Executors.newFixedThreadPool(MAX_THREADS);
		long starttime = System.currentTimeMillis();
		LongAdderTask task = new LongAdderTask(starttime);
		for(int i = 0; i < TASK_COUNT; i++) {
			executor.submit(task);
		}
		cdlAdder.await(); 
		executor.shutdown();
	}

	public static void main(String[] args) throws InterruptedException {
		LongAdderDemo demo = new LongAdderDemo();
		// 1.普通long测试
//		demo.testSync();
		// 2、Atomic long测试
//		demo.testAtomic();
		// 3、LongAdder测试
		demo.testLongAdder();

	}

}
