package com.chj.thread.capt12.completeableFuture;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import com.chj.thread.tools.SleepTools;

/**
 * CompletableFutureʹ�÷���
 * @author chenhuajing
 *
 */
public class CompleteableFutureDemo {
	
	static class GetResult extends Thread{
		CompletableFuture<Integer> cf;

		public GetResult(String threadName, CompletableFuture<Integer> cf) {
			super(threadName);
			this.cf = cf;
		}

		@Override
		public void run() {
			try {
                System.out.println("waiting result.....");
                System.out.println(this.getName() + ": " + cf.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
		}
	}

	
	public static void main(String[] args) throws IOException {
		final CompletableFuture<Integer> cf = new CompletableFuture<>();
		new GetResult("Client1",cf).start();
		new GetResult("Client2",cf).start();
		System.out.println("sleeping");
	    SleepTools.second(2);
	    cf.complete(100);
	    cf.completeExceptionally(new Exception());
	    System.in.read();
	}

}
