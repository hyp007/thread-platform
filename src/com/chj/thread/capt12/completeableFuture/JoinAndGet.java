package com.chj.thread.capt12.completeableFuture;

import java.util.concurrent.CompletableFuture;

/**
 * get和join方法的区别
 * @author chenhuajing
 *
 */
public class JoinAndGet {
	/*
	 * getNow有点特殊，如果结果已经计算完则返回结果或者抛出异常，否则返回给定的valueIfAbsent值。
	 * join返回计算的结果或者抛出一个unchecked异常(CompletionException)，它和get对抛出的异常的处理有些细微的区别
	 */
	public static void main(String[] args) {
		 CompletableFuture<Integer> future = CompletableFuture.supplyAsync(()->{
	         int i = 1/0;
	         return 100;
	     });
		//future.get();
		 future.join();
	}

}
