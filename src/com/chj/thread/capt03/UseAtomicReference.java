package com.chj.thread.capt03;

import java.util.concurrent.atomic.AtomicReference;

/**
 * 演示引用类型的原子操作类
 * @author Administrator
 *
 */
public class UseAtomicReference {
	
	static AtomicReference<UserInfo> userRef = new AtomicReference<UserInfo>();

	public static void main(String[] args) {
		UserInfo user = new UserInfo("Mark", 15);// 要修改的实体的实例
		userRef.set(user);
		// 要变化的新实例TODO Auto-generated method stub
		UserInfo updateUser = new UserInfo("Bill", 17);
		userRef.compareAndSet(user, updateUser);
		System.out.println(userRef.get().getAge());
		System.out.println(userRef.get().getName());
		System.out.println(user.getAge());
		System.out.println(user.getName());
	}

	//定义一个实体类
    static class UserInfo {
        private String name;
        private int age;
        public UserInfo(String name, int age) {
            this.name = name;
            this.age = age;
        }
        public String getName() {
            return name;
        }
        public int getAge() {
            return age;
        }
    }

}
