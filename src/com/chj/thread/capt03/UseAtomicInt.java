package com.chj.thread.capt03;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * 	原子操作是不能分割得整体，没有其他线程能够中断或检查正在源自操作的变量
 * @author Administrator
 *
 */
public class UseAtomicInt extends Thread{

	static AtomicInteger count = new AtomicInteger(10);
	
	static AtomicInteger count2 = new AtomicInteger(0);


	@Override
	public void run() {
		for(int i=0; i<10000; i++) {
			System.out.println(count2.incrementAndGet());
		}
	}


	public static void main(String[] args) {
		//
		System.out.println(count.get());
		// 10 --> out -->11
		System.out.println("getAndIncrement=="+count.getAndIncrement());
		// 11--> 12 --out
		System.out.println("getAndIncrement=="+count.incrementAndGet());
		
		// 通过原子操作实现累加
		UseAtomicInt atomic = new UseAtomicInt();
		new Thread(atomic).start();
		new Thread(atomic).start();
		new Thread(atomic).start();

	}

}
