package com.chj.thread.other.tree;

/**
 * 二叉平衡树，AVL树
 * @author Administrator
 *
 */
public class AVLTree {
	
	// inner class for tree node
	public static class Node{
		int data;
		Node leftChild; //左子节点
		Node rightChild; //又子节点
		
		int height; // 记录该节点所在的高度

		public Node(int data) {
			super();
			this.data = data;
		}
	}
	
	// 获取节点高度
	public static int getHeight(Node p) {
		return p ==null ? -1 : p.height;
	}
	
	public static void printTree(Node root) {
		System.out.println(root.data);
		if(root.leftChild != null) {
			System.out.println("leftChild:");
			printTree(root.leftChild);
		}
		
		if(root.rightChild != null) {
			System.out.println("rightChild:");
			printTree(root.rightChild);
		}
	}
	
	/**
	 * AVL树的插入方法
	 * @param root
	 * @param data
	 * @return
	 */
	public static Node insert(Node root, int data) {
		if (root == null) {
            root = new Node(data);
            return root;
        }
		//  插入到其左子树上
		if(data <= root.data) {
			root.leftChild = insert(root.leftChild,data);
			// 平衡调整
			if(getHeight(root.leftChild) - getHeight(root.rightChild) >1) {
				// 插入节点在失衡结点的左子树的左边
				if(data <= root.leftChild.data) {
					 System.out.println("LL旋转");
					 root = LLRotate(root); // LL旋转调整
				}else {// 插入节点在失衡结点的左子树的右边
					System.out.println("LR旋转");
                    root = LRRotate(root);
				}
			}
			
		}else {// 插入到其右子树上
			root.rightChild = insert(root.rightChild,data);
			// 平衡调整
			if(getHeight(root.rightChild) - getHeight(root.leftChild) >1) {
				//插入节点在失衡结点的右子树的左边
				if(data <= root.rightChild.data) {
					System.out.println("RL旋转");
                    root = RLRotate(root);
				}else {//插入节点在失衡结点的右子树的右边
					System.out.println("RR旋转");
                    root = RRRotate(root);
				}
			}
		}
		
		return root;
	}


	/*
     * LL旋转
     * 左旋示意图(对结点20进行左旋)
     *      30                       20
     *     /  \                     /  \
     *    20  40                  10   30
     *   /  \      --LL旋转-       /   /  \
     *  10   25                  5   25   40
     *  /
     * 5
     */
	private static Node LLRotate(Node root) {// 30为失衡点
		// 失衡点的左子树的根结点20作为新的结点
		Node leftSubtree = root.leftChild;
		// 将新节点的右子树25成为失衡点30的左子树
		root.leftChild = leftSubtree.rightChild;
		// 将失衡点30作为新结点的右子树
		leftSubtree.rightChild = root;
		// 重新设置失衡点30和新节点20的高度
		root.height = Math.max(getHeight(root.leftChild), getHeight(root.rightChild)) + 1;
		leftSubtree.height = Math.max(getHeight(leftSubtree.leftChild), root.height) + 1;
		return leftSubtree;
	}
	
	   /*
     * RR旋转
     * 右旋示意图(对结点30进行左旋)
     *      20                          30
     *     /  \                        /  \
     *    10  30                     20   40
     *       /  \      --RR旋转-     /  \   \
     *      25  40                 10  25  50
     *           \
     *           50
     */
	private static Node RRRotate(Node root) {// 20为失衡点
		// 失衡点的右子树的根结点30作为新的结点
		Node rightBubtree = root.rightChild;
		// 将新节点的左子树25成为失衡点20的右子树
		root.rightChild = rightBubtree.leftChild;
		// 将失衡点20作为新结点的左子树
		rightBubtree.leftChild = root;
		// 重新设置失衡点20和新节点30的高度
		root.height = Math.max(getHeight(root.leftChild), getHeight(root.rightChild)) + 1;
		rightBubtree.height = Math.max(getHeight(rightBubtree.leftChild), getHeight(rightBubtree.rightChild)) + 1;
		// 新的根节点取代原失衡点的位置
		return rightBubtree;
	}

	// LR旋转
	private static Node LRRotate(Node root) {
		// 先将失衡点p的右子树进行LL平衡旋转
		root.rightChild = LLRotate(root.rightChild);
		// 再将失衡点p进行RR平衡旋转并返回新节点代替原失衡点p
		return RRRotate(root);
	}
	
	// RL平衡旋转
	private static Node RLRotate(Node root) {
		// 先将失衡点p的左子树进行RR旋转
		root.leftChild = RRRotate(root.leftChild); 
		// 再将失衡点p进行LL平衡旋转并返回新节点代替原失衡点p
        return LLRotate(root);
	}
	
	
    public static void main(String[] args) {
        Node root = null;
        root = insert(root,30);
        root = insert(root,20);
        root = insert(root,40);
        root = insert(root,10);
        root = insert(root,25);
        //插入节点在失衡结点的左子树的左边
        root = insert(root,5);
        //打印树，按照先打印左子树，再打印右子树的方式
        printTree(root);

    }

	
}
