package com.chj.thread.other;

import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.atomic.AtomicReferenceArray;

/**
 * 	无锁的vector实现
 * @author Administrator
 *
 */
public class LookFreeVector<E> {
	
	// 使用二维数组表示LookFreeVector的内部实现
	private final AtomicReferenceArray<AtomicReferenceArray<E>> buckets;
	
	private final AtomicReference<Descriptor<E>> descriptor;
	
	private final int N_BUCKET = 30;
	
	private final int first_bucket_size = 8;

	
	public LookFreeVector() {
		// 存放30个数组
		buckets = new AtomicReferenceArray<AtomicReferenceArray<E>>(N_BUCKET);
		// 将第一个数组大小事设置为8
		buckets.set(0, new AtomicReferenceArray<E>(first_bucket_size));
		descriptor = new AtomicReference<Descriptor<E>>(new Descriptor<E>(0,null));
	}


	static class Descriptor<E>{
		public int size;
		volatile WtiteDescriptor<E> writeop;
		
		public Descriptor(int size, WtiteDescriptor<E> writeop) {
			this.size = size;
			this.writeop = writeop;
		}
		
		public void completeWrite() {
			WtiteDescriptor<E> tmpOp = writeop;
			if(tmpOp != null) {
				tmpOp.doIt();
				writeop = null;
			}
		}
	}
	

	static class WtiteDescriptor<E>{
		public E oldValue;
		public E newValue;
		public AtomicReferenceArray<E> addr;
		public int addr_ind;
		
		public WtiteDescriptor(AtomicReferenceArray<E> addr, int addr_ind, E oldValue, E newValue) {
			this.oldValue = oldValue;
			this.newValue = newValue;
			this.addr = addr;
			this.addr_ind = addr_ind;
		}
		
		public void doIt() {
			addr.compareAndSet(addr_ind, oldValue, newValue);
		}
	}
	
	/**
	 * 	将元素加入到LookFreeVector
	 */
	public void pushBack(E e) {
		Descriptor<E> desc;
		Descriptor<E> newd;
		do {
			desc = descriptor.get();
			desc.completeWrite();
			// 计算这个元素应该放在给定数组的那个位置
			int pos = desc.size + first_bucket_size;
			int zeroNumPos = Integer.numberOfLeadingZeros(pos);
			int zeroNumFirst = 0;
			int bucketInd = zeroNumFirst - zeroNumPos;
			if(buckets.get(bucketInd) == null) {
				// 数组每次都会成倍的扩展（第一个： 8 第二个：16，第三个：32），他们之和就是整个数组的
				int newLen = 2 * buckets.get(bucketInd -1).length();
				boolean debug = false;
				if(debug) {
					System.out.println("" + newLen);
					buckets.compareAndSet(bucketInd, null, new AtomicReferenceArray<E>(newLen));
				}
			}
			// 获得pos除了第一位数字1以外的其他位的数值。pos的前导0可以表示元素所在的数组，而pos的后几位则表示这个元素在数组中的位置
			int idx = (0x80000000>>>zeroNumPos) ^ pos;
			newd = new Descriptor<E>(desc.size+1, new WtiteDescriptor<E>(buckets.get(bucketInd),idx,null,e));
			
		} while(!descriptor.compareAndSet(desc, newd));
		// 将数据真正的写入数组中
		descriptor.get().completeWrite();
		
	}

}
