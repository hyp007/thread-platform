package com.chj.thread.capt01;

/**
 *	 守护线程 和主线程共死，finally不能保证一定执行
 * @author Administrator
 *
 */
public class DaemonThread {
	
	private static class UseThread extends Thread{
		
		@Override
		public void run() {
			try {
				String threadName = Thread.currentThread().getName();
				while(!isInterrupted()) {// 子线程会中断
					System.out.println("thread----"+threadName+" is extends running ----");
				}
				System.out.println(threadName+" interrupt flag =="+isInterrupted());
			} finally {
				// 使用了收获线程，finally不一定起作用
				System.out.println("finally-----------------");
			}
		}
		
	}
	

	public static void main(String[] args) throws InterruptedException {
		Thread useThread = new UseThread();
//		useThread.setPriority(10); // 1~10 优先级
		// 设置开启守护线程
//		useThread.setDaemon(true);
		useThread.start();
		Thread.sleep(20);
		useThread.interrupt();// 休眠20毫秒以后中断该线程
		

	}

}
