package com.chj.thread.capt01;

/**
 * 	演示ThreadLocal的使用
 * ThreadLocal主要解决的就是每个线程绑定自己的值，可以将ThreadLocal比喻成全局存放数据的盒子，盒子中可以存储每个线程的私有数据
 *
 */
public class UseThreadLocal {
	
	// 可以理解为一个map, 初始化覆盖initialValue方法，解决第一次获取返回null的问题
	static ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>() {
		@Override
		protected Integer initialValue() {
			return 1;
		}
	};
	
	// 运行3个线程
	public void StartThreadArray() {
		Thread[] runs = new Thread[3];
		  for(int i=0;i<runs.length;i++){
			  runs[i] = new Thread(new TestThread(i));
		  }
		  for(int i=0;i<runs.length;i++){
	          runs[i].start();
	      }
	}
	
	/**
	 * 测试线程， 线程得工作是将threadLocal变量得值改变，并回写，看线程之间是否相互影响
	 *
	 */
	public static class TestThread implements Runnable {
		int id;
        public TestThread(int id){
            this.id = id;
        }
        
		@Override
		public void run() {
			System.out.println(Thread.currentThread().getName()+":start");
			// 获取变量值
			Integer s = threadLocal.get();
			s = s+id;
			threadLocal.set(s);
			System.out.println(Thread.currentThread().getName()+":" +threadLocal.get());
			//threadLaocl.remove();
		}

	}
	
	 public static void main(String[] args){
	    	UseThreadLocal test = new UseThreadLocal();
	        test.StartThreadArray();
	    }

}
