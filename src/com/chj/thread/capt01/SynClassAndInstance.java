package com.chj.thread.capt01;

import com.chj.thread.tools.SleepTools;

/**
 * 	演示对象锁和类锁
 * @author Administrator
 *
 */
public class SynClassAndInstance {
	
	// 使用类锁的线程
    private static class SynchronizedClass extends Thread{
        @Override
        public void run() {
        	String threadName = Thread.currentThread().getName();
            System.out.println(threadName + "----Test Synchronized Class is running...");
            synchronizedClass();
        }
    }
    // 类锁方法：实际上是锁类的class对象
    private static synchronized void synchronizedClass() {
    	 SleepTools.second(1);
         System.out.println("synchronizedClass going-----------");
         SleepTools.second(1);
         System.out.println("synchronizedClass end！！");
    }
    
    //---------------------------------------------------------
    
    // 使用对象所线程
    private static class Instace01Synchronized  implements Runnable{
    	private SynClassAndInstance synClassAndInstance;
    	
		public Instace01Synchronized(SynClassAndInstance synClassAndInstance) {
			this.synClassAndInstance = synClassAndInstance;
		}

		@Override
		public void run() {
			String threadName = Thread.currentThread().getName();
			System.out.println(threadName+" ------TestInstance01 is running..."+synClassAndInstance);
			synClassAndInstance.instance();
		}
    }
    // 锁 对象
	public void instance() {
		 SleepTools.second(3);
	     System.out.println("Instace01Synchronized is going..."+this.toString());
	     SleepTools.second(3);
	     System.out.println("Instace01Synchronized ended "+this.toString());
	}
    
	// 使用对象所线程2
    private static class Instace02Synchronized  implements Runnable{
    	private SynClassAndInstance synClassAndInstance;
    	
		public Instace02Synchronized(SynClassAndInstance synClassAndInstance) {
			this.synClassAndInstance = synClassAndInstance;
		}

		@Override
		public void run() {
			String threadName = Thread.currentThread().getName();
			System.out.println(threadName + " --------TestInstance02 is running..."+synClassAndInstance);
			synClassAndInstance.instance2();
		}
    }
    // 锁 对象2
	public void instance2() {
		SleepTools.second(3);
	    System.out.println("Instace02Synchronized is going..."+this.toString());
	    SleepTools.second(3);
	    System.out.println("Instace02Synchronized ended "+this.toString());
	}
    
    
    public static void main(String[] args) {
    	SynClassAndInstance synClzAndInst = new SynClassAndInstance();
    	Thread t1 = new Thread(new Instace01Synchronized(synClzAndInst));
    	
    	SynClassAndInstance synClzAndInst2 = new SynClassAndInstance();
    	Thread t2 = new Thread(new Instace02Synchronized(synClzAndInst2));
    	
    	t1.start();
    	t2.start();
    	
    	// 类锁
    	SynchronizedClass synClass = new SynchronizedClass();
    	synClass.start();
    	SleepTools.second(1);
    	
    }
    

}
