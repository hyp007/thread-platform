package com.chj.thread.capt01;

/**
 * run()和start() ：run方法就是普通对象的普通方法，只有调用了start()后，
 * Java才会将线程对象和操作系统中实际的线程进行映射，再来执行run方法
 * @author Administrator
 *
 */
public class StartAndRun {
	
	private static class UseThread extends Thread{
		public UseThread(String name) {
			super(name);
		}
		
		@Override
		public void run() {
			String threadName = Thread.currentThread().getName();
			int i = 100;
			while(i > 0) {//
				System.out.println("thread----"+threadName+" is run: "+i--);
			}
//			System.out.println(threadName+" interrupt flag =="+isInterrupted());
		}
		
	}
	

	public static void main(String[] args) {
		Thread enThread = new UseThread("enThread");
		enThread.setName("becalled");
		// 如果直接调用 等于调用普通类得一个方法一样
		enThread.run();
		// 只有调用start方法得时候 线程才会被当作一个独立得线程方法去执行
//		enThread.start();
//		Thread.sleep(20);
//		enThread.interrupt();// 休眠20毫秒以后中断该线程
		

	}

}
