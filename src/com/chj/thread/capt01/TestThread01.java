package com.chj.thread.capt01;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class TestThread01 {
	
	private static class UseThread extends Thread{
		
	}
	
	private static class UseRun implements Runnable {
		@Override
		public void run() {
			System.out.println("Runnable --thread----");
			
		}
	}
	
	private static class UseCallable implements Callable<String> {

		@Override
		public String call() throws Exception {
			System.out.println("Callable<String> --thread----");
			return "";
		}
		
	}
	
	
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		// 打印出来的都是守护线程（和主线程共死，finally不能保证一定执行）
		ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
		ThreadInfo[]  threadInfo = threadMXBean.dumpAllThreads(false, false);
		for(ThreadInfo th: threadInfo) {
			System.out.println("["+th.getThreadId()+"]"+" " +th.getThreadName());
		}
		
		
		UseRun useRun = new UseRun();
		new Thread(useRun).start();
		//
		Thread t = new Thread(useRun);
		t.interrupt();
		
		UseCallable useCall = new UseCallable();
		FutureTask<String> futureTask = new FutureTask<>(useCall);
		new Thread(futureTask).start();
		System.out.println(futureTask.get());
		
		
	}


	
	
}
