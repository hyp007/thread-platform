package com.chj.thread.capt01;

public class EndRunnable {
	
	private static class UseRunnable implements Runnable{
		
		@Override
		public void run() {
			String threadName = Thread.currentThread().getName();
//			while(true()) {// 子线程永远不会中断
			while(!Thread.currentThread().isInterrupted()) {// 子线程会中断
				System.out.println("thread----"+threadName+" is run ----");
			}
			System.out.println(threadName+" interrupt flag =="
					+Thread.currentThread().isInterrupted());
		}
		
	}
	

	public static void main(String[] args) throws InterruptedException {
		UseRunnable useRunnable = new UseRunnable();
		Thread endThread = new Thread(useRunnable,"endThread");
		endThread.start();
		Thread.sleep(20);
		// 休眠20毫秒以后中断该线程
		endThread.interrupt();
		

	}

}
