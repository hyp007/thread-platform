package com.chj.thread.capt04.aqs;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.AbstractQueuedSynchronizer;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * 	实现我们自己的 可重入锁
 * @author Administrator
 *
 */
public class ReenterSelfLock implements Lock{
	
	/**
	 * 	静态内部类 ，自定义同步器
	 */
	private static class Sync extends AbstractQueuedSynchronizer{

		// 判断该线程是否正在独占资源。只有用到condition才需要去实现它。
		@Override
		protected boolean isHeldExclusively() {
			return getState() > 0;
		}
			
		// 当状态为0的时候获取锁，独占方式：尝试获取资源，成功则返回true，失败则返回false。
		@Override
		protected boolean tryAcquire(int arg) {
			if(compareAndSetState(0, 1)) {
				// Sets the thread that currently owns exclusive access.
				setExclusiveOwnerThread(Thread.currentThread());
				return true;
			}else if(getExclusiveOwnerThread() == Thread.currentThread()) {
				setState(getState()+1);
				return true;
			}
			return false;
		}

		// 释放锁 将状态设置为0
		// 独占方式。尝试释放资源，成功则返回true，失败则返回false。
		@Override
		protected boolean tryRelease(int arg) {
			// 
			if(getExclusiveOwnerThread() != Thread.currentThread()) {
				throw new IllegalMonitorStateException();
			}
			// Returns the current value of synchronization state.
			if(getState() ==0) {
				throw new IllegalMonitorStateException();
			}
			// 
			setState(getState()-1);
			if(getState() ==0) {
				setExclusiveOwnerThread(null);
			}
			return true;
		}
		
		// tryAcquireShared(int)：共享方式：尝试获取资源。负数表示失败；0表示成功，但没有剩余可用资源；正数表示成功，且有剩余资源。
		// tryReleaseShared(int)：共享方式：尝试释放资源，如果释放后允许唤醒后续等待结点返回true，否则返回false。

		// 返回一个Condition，每个condition都包含了一个condition队列
        Condition newCondition() {
            return new ConditionObject();
        }
		
	}
	
	// 仅需要将操作代理到Sync上即可
	private final Sync sync = new Sync();

	/**
	 * 	获取锁，如果锁已经被占用，则等待
	 */
	@Override
	public void lock() {
		System.out.println(Thread.currentThread().getName()+" ready get lock");
		sync.acquire(1);
		System.out.println(Thread.currentThread().getName()+" already got lock");
	}

	/**
	 * 	尝试获取锁，如果成功返回true，失败返回false,该方法不等待，直接返回
	 */
	@Override
	public boolean tryLock() {
		return sync.tryAcquire(1);
	}
	
	/*
	 * 	释放锁
	 */
	@Override
	public void unlock() {
		System.out.println(Thread.currentThread().getName()+" ready release lock");
		sync.release(1);
		System.out.println(Thread.currentThread().getName()+" already released lock");
	}

	/**
	 * 	在给定时间内尝试获取锁
	 */
	@Override
	public boolean tryLock(long timeout, TimeUnit unit) throws InterruptedException {
		return sync.tryAcquireNanos(1, unit.toNanos(timeout));
	}

	/*
	 * 	获取锁，但优先响应中断
	 */
	@Override
	public void lockInterruptibly() throws InterruptedException {
		sync.acquireInterruptibly(1);
	}
	
	// 
	public boolean isLocked() {
		return sync.isHeldExclusively();
	}

	public boolean hasQueuedThreads() {
		return sync.hasQueuedThreads();
	}

	@Override
	public Condition newCondition() {
		return sync.newCondition();
	}

}
