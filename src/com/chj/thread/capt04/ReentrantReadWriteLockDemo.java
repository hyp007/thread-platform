package com.chj.thread.capt04;

import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * 	读写锁表示有两个锁：读锁也称共享锁，也就是多个读锁之间不互斥，写锁也称排他锁，写锁之间互斥
 * 	多个线程可以同时进行读取操作，但是同一时刻只允许要给线程进行写入操作
 *	读与读之间不阻塞，读写阻塞，写写阻塞
 */
public class ReentrantReadWriteLockDemo {
	
	private static Lock lock = new ReentrantLock();
	private static ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	private static Lock readLock = readWriteLock.readLock();
	private static Lock writeLock = readWriteLock.writeLock();
	
	private int value;
	
	public Object handleRead(Lock lock) throws InterruptedException {
		lock.lock();
		try {
			Thread.sleep(1000);
			System.out.println("handleRead=="+Thread.currentThread().getName()+"value=="+value);
			return value;
		} finally {
			lock.unlock();
		}
	}
	
	public void handleWrite(Lock lock,int index) throws InterruptedException {
		lock.lock();
		try {
			Thread.sleep(1000);
			System.out.println("handleWrite=="+Thread.currentThread().getName()+" index=="+index);
			value = index;
		} finally {
			lock.unlock();
		}
	}
	
	
	public static void main(String[] args) {
		final ReentrantReadWriteLockDemo demo = new ReentrantReadWriteLockDemo();
		
		Runnable readRunnable = new Runnable() {
			@Override
			public void run() {
				try {
					demo.handleRead(readLock);
//					demo.handleRead(lock);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		
		Runnable writeRunnable = new Runnable() {
			@Override
			public void run() {
				try {
					demo.handleWrite(writeLock,new Random().nextInt());
//					demo.handleWrite(lock,new Random().nextInt());
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		};
		
		for(int i = 0; i < 18; i++) {
			new Thread(readRunnable).start();
		}
		
		for(int i = 18; i < 20; i++) {
			new Thread(writeRunnable).start();
		}
	}

}
