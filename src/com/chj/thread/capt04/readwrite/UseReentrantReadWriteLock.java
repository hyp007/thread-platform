package com.chj.thread.capt04.readwrite;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import com.chj.thread.tools.SleepTools;

/**
 * 	用读写锁来实现商品服务接口
 * @author Administrator
 *
 */
public class UseReentrantReadWriteLock implements GoodsService{
	
	private GoodsInfo goodsInfo;
	
	private final ReadWriteLock rwlocak = new ReentrantReadWriteLock();
    private final Lock readLock = rwlocak.readLock();//读锁
    private final Lock writeLock = rwlocak.writeLock();//写锁
    
	public UseReentrantReadWriteLock(GoodsInfo goodsInfo) {
		this.goodsInfo = goodsInfo;
	}


	@Override
	public GoodsInfo getNum() {
		readLock.lock();
		try {
			SleepTools.ms(5);
	        return this.goodsInfo;
		}finally {
			readLock.unlock();
		}
		
	}

	@Override
	public void setNum(int number) {
		writeLock.lock();
		try {
			SleepTools.ms(5);
			goodsInfo.changeNumber(number);
		}finally {
			writeLock.unlock();
		}
	}

}
