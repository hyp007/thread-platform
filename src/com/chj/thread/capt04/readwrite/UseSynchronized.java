package com.chj.thread.capt04.readwrite;

import com.chj.thread.tools.SleepTools;

/**
 *	 用内置锁来实现商品服务接口
 * @author Administrator
 *
 */
public class UseSynchronized implements GoodsService{

	private GoodsInfo goodsInfo;
	
	public UseSynchronized(GoodsInfo goodsInfo) {
		this.goodsInfo = goodsInfo;
	}

	@Override
	public synchronized GoodsInfo getNum() {
		SleepTools.ms(5);
		return this.goodsInfo;
	}

	@Override
	public synchronized void setNum(int number) {
		SleepTools.ms(5);
		goodsInfo.changeNumber(number);
	}

}
