package com.chj.thread.capt04.readwrite;

import java.util.Random;

import com.chj.thread.tools.SleepTools;

/**
 *	 对商品进行业务的应用
 * @author Administrator
 *
 */
public class BusinessApp {
	
	static final int readWriteRatio = 10;//读写线程的比例
    static final int minthreadCount = 3;//最少线程数
    
    //读操作
    private static class ReadThread implements Runnable{

        private GoodsService goodsService;
        public ReadThread(GoodsService goodsService) {
            this.goodsService = goodsService;
        }
        
		@Override
		public void run() {
			long start = System.currentTimeMillis();
            for(int i=0;i<100;i++){//操作100次
            	SleepTools.ms(10);
                goodsService.getNum();
            }
            System.out.println(Thread.currentThread().getName()
            		+"read读取商品数据耗时："+(System.currentTimeMillis()-start)+"ms");

		}
    }
    
    //写操做
    private static class WriteThread implements Runnable{

        private GoodsService goodsService;
        public WriteThread(GoodsService goodsService) {
            this.goodsService = goodsService;
        }

        @Override
        public void run() {
            long start = System.currentTimeMillis();
            Random r = new Random();
            for(int i=0;i<10;i++){//操作10次
            	SleepTools.ms(50);
                goodsService.setNum(r.nextInt(10));
            }
            System.out.println(Thread.currentThread().getName()
            		+"write--写商品数据耗时："+(System.currentTimeMillis()-start)+"ms---------");

        }
    }
    

	public static void main(String[] args) {
		GoodsInfo goodsInfo = new GoodsInfo("Cup",100000,10000);
		//
		GoodsService goodsService = new UseReentrantReadWriteLock(goodsInfo);
//		GoodsService goodsService = new UseSynchronized(goodsInfo);
		for(int i = 0;i<minthreadCount;i++){
			for(int j=0; j< readWriteRatio; j++) {
				Thread readThread = new Thread(new ReadThread(goodsService));
				readThread.start();
			}
			Thread writeThread = new Thread(new WriteThread(goodsService));
			SleepTools.ms(50);
			writeThread.start();
	    }
	}

}
