package com.chj.thread.capt09;

import java.util.Random;

import com.chj.thread.capt09.vo.ITaskProcesser;
import com.chj.thread.capt09.vo.TaskResult;
import com.chj.thread.capt09.vo.TaskResultType;
import com.chj.thread.tools.SleepTools;

/**
 * 一个实际任务类，将数值加上一个随机数，并休眠随机时间
 * @author Administrator
 *
 */
public class MyTask implements ITaskProcesser<Integer, Integer> {

	@Override
	public TaskResult<Integer> executeTask(Integer data) {
		Random r = new Random();
		int flag = r.nextInt(500);
		SleepTools.ms(flag);
		// 正常处理的情况
		if(flag <= 300) {
			Integer returnValue = data.intValue() + flag;
			return new TaskResult<Integer>(TaskResultType.success,returnValue);
		}else if(flag > 301 && flag <= 450) {// 处理失败的情况
			return new TaskResult<Integer>(TaskResultType.failure,-1,"porcess failure!");
		}else {//发生异常的情况
			try {
				throw new RuntimeException("异常发生了！！");
			}catch(Exception e){
				return new TaskResult<Integer>(TaskResultType.exception,-1,e.getMessage());
			}
			
		}
	}

}
