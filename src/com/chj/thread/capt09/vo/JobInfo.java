package com.chj.thread.capt09.vo;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.atomic.AtomicInteger;

import com.chj.thread.capt09.CheckJobProcesser;

/**
 * 	提交给框架执行的工作实体类
 * 	工作：表示本批次需要处理的同性质任务(Task)的一个集合
 * @author Administrator
 *
 */
public class JobInfo<R> {
	
	//工作名，用以区分框架中唯一的工作
	private final String jobName;
	// 工作中任务的长度
	private final int jobSize;
	// 处理工作中任务的处理器
	private final ITaskProcesser<?,?> taskProcesser;
	// 任务的成功次数
	private AtomicInteger successCount;
	// 工作中任务目前已经处理的次数
	private AtomicInteger taskProcessCount;
	
	// 存放每个任务的处理结果，供查询用
	private LinkedBlockingDeque<TaskResult<R>> taskResultQueues;

	// 保留的工作的结果信息供查询的时长
	private final long expireTime;
	
	private static CheckJobProcesser checkJob = CheckJobProcesser.getInstance();
	
	public int getJobSize() {
		return jobSize;
	}

	public ITaskProcesser<?, ?> getTaskProcesser() {
		return taskProcesser;
	}

	public JobInfo(String jobName, int jobSize, ITaskProcesser<?, ?> taskProcesser, long expireTIme) {
		super();
		this.jobName = jobName;
		this.jobSize = jobSize;
		this.taskProcesser = taskProcesser;
		successCount = new AtomicInteger(0);
		taskProcessCount = new AtomicInteger(0);
		taskResultQueues = new LinkedBlockingDeque<TaskResult<R>>(jobSize);
		this.expireTime = expireTIme;
	}
	
	// 获取任务成功数
	public int getSucessCount() {
		return successCount.get();
	}
	// 获取任务处理数量
	public int getTaskProcessCount() {
		return taskProcessCount.get();
	}
	// 返回处理是失败的任务数
	public int getFailedCount() {
		return taskProcessCount.get() - successCount.get();
	}
	
	/**
	 * 	提供工作整体进度信息
	 * @return
	 */
	public String getTotalProcess() {
		 return "Success["+successCount.get()+"]/Current["+taskProcessCount.get()+"] Total["+jobSize+"]";
	}
	
	/**
	 * 	提供工作中每个任务的处理结果
	 * @return
	 */
	public List<TaskResult<R>>  getTaskDetail(){
		List<TaskResult<R>> taskResultList = new LinkedList<>();
		TaskResult<R> taskResult;
		while((taskResult = taskResultQueues.pollFirst()) != null) {
			taskResultList.add(taskResult);
		}
		return taskResultList;
	}
	
	/**
	 *	 每个任务处理完成后，记录任务的处理结果，因为从业务应用的角度来说， 对查询任务进度数据的一致性要不高我们保证最终一致性即可，无需对整个方法加锁
	 * @param taskResult
	 */
	public void addTaskResult(TaskResult<R> taskResult) {
		// 如果任务处理成功，成功数量+1
		if(TaskResultType.success.equals(taskResult.getResultType())) {
			successCount.incrementAndGet();
		}
		// 任务总是累加
		taskProcessCount.incrementAndGet();
		// 将任务放到队列尾部
		taskResultQueues.addLast(taskResult);
		// 放入队列，经过expireTime时间后，从整个框架中移除
		if(taskProcessCount.get() == jobSize) {
			checkJob.pushJob(jobName, expireTime);
		}
		
	}
	
	
	
}
