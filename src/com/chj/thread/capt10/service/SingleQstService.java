package com.chj.thread.capt10.service;

import com.chj.thread.capt10.bussiness.SL_QuestionBank;

/**
 * 模拟解析题目文本，下载图片等操作，返回解析后的文本
 * @author Administrator
 *
 */
public class SingleQstService {

	/**
	 * 普通对题目进行处理
	 * @param quetstionId 题目id
	 * @return 题目解析后的文本
	 */
	public static Object makeQuestion(Integer quetstionId) {
		// TODO Auto-generated method stub
		return QstService.makeQuestion(quetstionId,SL_QuestionBank.getQuetion(quetstionId).getDetail());
	}

}
