package com.chj.thread.capt10.service;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;

import com.chj.thread.capt10.bussiness.SL_Busi;
import com.chj.thread.capt10.vo.SrcDocVo;
import com.chj.thread.capt10.vo.TaskResultVo;

/**
 * 处理文档的服务
 * @author Administrator
 *
 */
public class ProduceDocService {

	/**
	 * 将待处理文档处理为本地实际文档
	 * @param doc
	 * @return 实际文档在本地的存储位置
	 */
	public static String makeDoc(SrcDocVo pendingDocVo) {
		System.out.println("开始处理文档："+ pendingDocVo.getDocName());
	    StringBuffer sb = new StringBuffer();
	    for(Integer quetstionId : pendingDocVo.getQuestionList()) {
	    	sb.append(SingleQstService.makeQuestion(quetstionId));
	    }
		return "complete_"+System.currentTimeMillis()+"_"+ pendingDocVo.getDocName()+".pdf";
	}

	/**
	 * 上传文档到网络
	 * @param localName
	 * @return
	 */
	public static String upLoadDoc(String docFileName) {
		Random r = new Random();
		SL_Busi.business(9000+r.nextInt(400));
		return "http://www.xxxx.com/file/upload/"+docFileName;
	}

	/**
	 * 异步生成题目
	 * @return
	 * @throws ExecutionException 
	 * @throws InterruptedException 
	 */
	public static String makeDocAsyn(SrcDocVo pendingDocVo) throws InterruptedException, ExecutionException {
		System.out.println("开始处理文档："+ pendingDocVo.getDocName());
		// 每个题目的处理结果
		Map<Integer, TaskResultVo> questionResultMap = new HashMap<>();
		for(Integer questionId : pendingDocVo.getQuestionList()) {
			questionResultMap.put(questionId, ParalleQstService.makeQuestion(questionId));
		}
		StringBuffer sb = new StringBuffer();
		for(Integer questionId : pendingDocVo.getQuestionList()) {
			TaskResultVo taskResultVo = questionResultMap.get(questionId);
			sb.append(taskResultVo.getQuestionDetail() == null ? 
					taskResultVo.getQuestionFuture().get().getQuestionDetail() : taskResultVo.getQuestionDetail());
		}
		
		return "complete_"+System.currentTimeMillis()+"_"+ pendingDocVo.getDocName()+".pdf";
		
	}
	
}
