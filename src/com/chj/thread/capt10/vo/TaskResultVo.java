package com.chj.thread.capt10.vo;

import java.util.concurrent.Future;

/**
 * 并发处理返回的题目结果实体类
 * @author Administrator
 *
 */
public class TaskResultVo {
	
	private final String questionDetail;
	private final Future<QuestionInCacheVo> questionFuture;
	
	public TaskResultVo(String questionDetail) {
		this.questionDetail = questionDetail;
		this.questionFuture = null;
	}

	public TaskResultVo(Future<QuestionInCacheVo> questionFuture) {
		this.questionDetail = null;
		this.questionFuture = questionFuture;
	}

	public String getQuestionDetail() {
		return questionDetail;
	}

	public Future<QuestionInCacheVo> getQuestionFuture() {
		return questionFuture;
	}
	
	
	

}
